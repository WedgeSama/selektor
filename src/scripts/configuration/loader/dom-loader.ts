import {Configuration} from '../configuration';

export abstract class DomLoader {
    public static load($select: HTMLSelectElement, config: Configuration): Configuration {
        const loadedConfig: Configuration = Object.assign({
            templates: {},
        }, config);

        // Load placeholder.
        for (const attr of ['data-selektor-placeholder', 'data-placeholder', 'placeholder']) {
            if ($select.hasAttribute(attr)) {
                loadedConfig.placeholder = $select.getAttribute(attr);
                break;
            }
        }

        // Load multiple
        if ($select.hasAttribute('multiple')) {
            loadedConfig.multiple = true;
        }

        // Remote URL.
        if ($select.hasAttribute('data-selektor-remote-url')) {
            const url = $select.getAttribute('data-selektor-remote-url');
            if (undefined === loadedConfig.remote) {
                loadedConfig.remote = {
                    url: url,
                };
            } else if ('object' === typeof loadedConfig.remote) {
                loadedConfig.remote.url = url;
            }
        }

        // Remote value and label fields.
        for (const field of ['label', 'value']) {
            const attr = $select.getAttribute(`data-selektor-remote-${field}`);
            if (null !== attr) {
                if (undefined === loadedConfig.remote) {
                    loadedConfig.remote = {};
                }

                loadedConfig.remote[field + 'Field'] = attr;
            }
        }

        // Templates
        for (const template of ['main', 'option', 'optgroup', 'item', 'nooption']) {
            const attr = `data-selektor-template-${template}`;
            if ($select.hasAttribute(attr)) {
                loadedConfig.templates[template] = $select.getAttribute(attr);
            }
        }

        // CSS Classes
        let cssClasses = $select.getAttribute('data-selektor-class');
        if (null !== cssClasses) {
            cssClasses = cssClasses.trim();
            if (0 < cssClasses.length) {
                loadedConfig.templates.cssClasses = cssClasses;
            }
        }

        return loadedConfig;
    }
}
