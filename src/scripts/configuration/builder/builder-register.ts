import {Configuration} from '../configuration';

export type ConfiguratorBuilder = ($selektor: HTMLSelectElement) => Configuration|null;

export class BuilderRegister {
    private builders: Array<ConfiguratorBuilder> = [];

    public buildConfiguration($selektor: HTMLSelectElement): Configuration|null {
        for (const builder of this.builders) {
            const config = builder($selektor);

            if (null !== config) {
                return config;
            }
        }

        return null;
    }

    public addBuilder(builder: ConfiguratorBuilder): BuilderRegister {
        this.builders.push(builder);
        return this;
    }
}
