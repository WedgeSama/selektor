import {Data} from '../data';

type queryBuilderFunction = (search: string, page: number) => string;
type dataOptionConverterFunction = (remoteData: any) => Array<Data<object>>;
type checkFinishedFunction = (options: any, remoteData: any, response: Response) => boolean;

export interface RemoteConfiguration {
    url?: string;
    valueField?: string;
    labelField?: string;
    queryBuilder?: queryBuilderFunction;
    dataOptionConverter?: dataOptionConverterFunction;
    checkFinished?: checkFinishedFunction;
}

export interface ValidRemoteConfiguration {
    url: string;
    valueField: string;
    labelField: string;
    queryBuilder: queryBuilderFunction;
    dataOptionConverter: dataOptionConverterFunction;
    checkFinished: checkFinishedFunction;
}
