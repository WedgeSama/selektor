import {Configuration, ValidConfiguration} from '../configuration';
import {TemplateValidator} from './template-validator';
import {RemoteValidator} from './remote-validator';

export abstract class Validator {
    public static validate(config: Configuration): ValidConfiguration {
        let loader = null;
        if (undefined !== config.virtualLoader) {
            loader = config.virtualLoader;
        }

        if (undefined !== config.remote) {
            if (null !== loader) {
                throw new Error('Cannot config both "virtualLoader" and "remote", choose one.');
            }
        }

        const validConfig: ValidConfiguration = {
            placeholder: config.placeholder ?? '',
            multiple: config.multiple ?? false,
            templates: TemplateValidator.validate(config.templates),
            remote: RemoteValidator.validate(config.remote),
            virtualLoader: loader,
        };

        if ('string' !== typeof validConfig.placeholder) {
            this.throwTypeError('placeholder', 'string');
        }

        return validConfig;
    }

    private static throwTypeError(option: string, type: string): void {
        throw new Error(`The option "${option}" must be of type "${type}.`);
    }
}
