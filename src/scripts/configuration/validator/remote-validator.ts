import {ValidatorUtils} from './validator-utils';
import {RemoteConfiguration, ValidRemoteConfiguration} from '../remote-configuration';
import {Data} from '../../data';

export abstract class RemoteValidator {
    public static validate(config: RemoteConfiguration|string|undefined): ValidRemoteConfiguration|null {
        if (undefined === config) {
            return null;
        }

        if ('string' === typeof config) {
            config = {
                url: config,
            };
        }

        const validConfig: ValidRemoteConfiguration = {
            url: '',
            valueField: 'value',
            labelField: 'label',
            queryBuilder: (search: string, page: number): string => `page=${page}&search=${encodeURI(search)}`,
            dataOptionConverter: (remoteData: any) => {
                if (!Array.isArray(remoteData)) {
                    throw new Error('Cannot use default "queryBuilder" configuration if the response data is not an array.');
                }

                const options: Array<Data<object>> = [];
                for (const item of remoteData) {
                    if ('string' === typeof item) {
                        options.push({
                            value: item,
                            label: item,
                        });
                    } else if ('object' === typeof item) {
                        const opt = {
                            value: '',
                            label: '',
                            data: item,
                        };

                        for (const [attr, field] of [[validConfig.valueField, 'value'], [validConfig.labelField, 'label']]) {
                            let value: string = null;
                            if (undefined !== item[attr]) {
                                value = RemoteValidator.convertToString(item[attr]);
                            }

                            if (null === value) {
                                throw new Error(`Using default "dataOptionConverter" configuration. Missing "${attr}" attribute in remote item object.`);
                            }

                            opt[field] = value;
                        }

                        options.push(opt);
                    }
                }

                return options;
            },
            checkFinished: (options: Array<any>): boolean => {
                return 0 === options.length;
            },
        };

        Object.assign<ValidRemoteConfiguration, RemoteConfiguration>(validConfig, config);

        for (const attr of ['url', 'valueField', 'labelField']) {
            if ('string' !== typeof validConfig[attr]) {
                ValidatorUtils.throwTypeError(`remote.${attr}`, 'string');
            }

            ValidatorUtils.throwEmptyError(validConfig[attr], `remote.${attr}`);
        }

        return validConfig;
    }

    private static convertToString(value: any): string|null {
        const type = typeof value;

        if ('string' === type) {
            return value;
        } else if ('number' === type) {
            return value.toString();
        }

        return null;
    }
}
