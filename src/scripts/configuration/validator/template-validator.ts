import {TemplateConfiguration, ValidTemplateConfiguration} from '../template-configuration';
import templates from '../../templating/templates';
import {ValidatorUtils} from './validator-utils';

export abstract class TemplateValidator {
    public static validate(config: TemplateConfiguration): ValidTemplateConfiguration {
        const validConfig: ValidTemplateConfiguration = Object.assign<object, object, TemplateConfiguration>({
            cssClasses: null,
        }, templates, config) as ValidTemplateConfiguration;

        for (const template of ['main', 'option', 'optgroup', 'item', 'nooption']) {
            if ('string' === typeof validConfig[template]) {
                if ('function' !== typeof global[validConfig[template]]) {
                    ValidatorUtils.throwTypeError(`templates.${template}`, 'string', `The global variable named "${validConfig[template]}" does not exist or isn't a function.`);
                }

                validConfig[template] = global[validConfig[template]];
            } else if ('function' !== typeof validConfig[template]) {
                ValidatorUtils.throwTypeError(`templates.${template}`, 'function');
            }
        }

        return validConfig;
    }
}
