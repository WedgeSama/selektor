export interface Data<T extends object> {
    value: string;
    label: string;
    data?: T;
}
