import {Option} from '../option';

export interface LoaderInterface<T extends object> {
    load(): Array<Option<T>>;
}
