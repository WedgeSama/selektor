import {LoaderInterface} from './loader';
import {Option} from '../option';

export class DomLoader<T extends object> implements LoaderInterface<T> {
    constructor(
        private $select: HTMLSelectElement,
    ) {
    }

    public load(): Array<Option<T>> {
        const options = [];
        for (const $opt of this.$select.children) {
            if ($opt instanceof HTMLOptionElement) {
                if (0 === $opt.value.trim().length) {
                    continue;
                }

                options.push(new Option<T>($opt));
            } else if ($opt instanceof HTMLOptGroupElement) {
                for (const $subOpt of $opt.children) {
                    if ($subOpt instanceof HTMLOptionElement) {
                        options.push(new Option<T>($subOpt));
                    }
                }
            }
        }

        return options;
    }
}
