import {Option} from './option';
import {DomLoader} from './loader/dom-loader';
import {EventEmitter} from '../event-emitter';
import {OptionManagerEvent} from './option-manager-event';

export class OptionManager<T extends object> {
    private options: Map<string, Option<T>> = new Map();
    private disableValues: Array<string> = [];
    private eventEmitter = new EventEmitter();
    private order: Array<Option<T>> = [];

    constructor(
        private $select: HTMLSelectElement,
        private multiple: boolean,
    ) {
        const loader = new DomLoader<T>(this.$select);
        for (const option of loader.load()) {
            this.addOption(option);
        }
    }

    public get selected(): Array<Option<T>>|Option<T>|null {
        const selected = Array.from(this.options.values()).filter((option: Option<T>) => option.selected);

        if (this.multiple) {
            return selected;
        }

        return selected[0] ?? null;
    }

    public clear(): void {
        for (const option of this.options.values()) {
            option.selected = false;
        }
    }

    public unselectLast(): void {
        if (0 === this.order.length) {
            return;
        }

        const option = this.order[this.order.length - 1];

        if (option) {
            option.selected = false;
        }
    }

    public getOptionFor($opt: HTMLOptionElement): Option<T>|null;
    public getOptionFor(value: string, label: string, selected?: boolean, data?: T): Option<T>|null;
    public getOptionFor(value: HTMLOptionElement|string, label?: string, selected?: boolean, data?: T): Option<T>|null {
        let key = value;
        if (key instanceof HTMLOptionElement) {
            key = key.value.trim();
        }

        if (this.options.has(key)) {
            const option = this.options.get(key);
            if (selected) {
                option.selected = true;
            }

            return option;
        }

        let option: Option<T>;
        if (value instanceof HTMLOptionElement) {
            option = new Option(value);
        } else {
            option = new Option(value, label, false, data);
        }

        if (-1 !== this.disableValues.indexOf(key)) {
            option.disabled = true;
        }

        this.addOption(option);

        if (selected) {
            option.selected = true;
        }

        return option;
    }

    public getHTMLOptionElementFor(option: Option<T>): HTMLOptionElement {
        let $option = this.$select.querySelector<HTMLOptionElement>(`option[value="${option.value}"]`);

        if (null === $option) {
            $option = document.createElement('option');
            $option.value = option.value;
            $option.label = option.label;
            $option.selected = option.selected;
            if (option.data) {
                $option.setAttribute('data-selektor-option-object', JSON.stringify(option.data));
            }
            this.$select.appendChild($option);
        }

        return $option;
    }

    /**
     * Add value to be disable (prevent selecting and unselect already selected).
     */
    public addDisableValue(value: string): void {
        value = value.trim();

        if (0 !== value.length && -1 === this.disableValues.indexOf(value)) {
            this.disableValues.push(value);
            this.applyDisabledOption(value);
        }
    }

    /**
     * Add multiple values to disable in one call.
     * @see addDisableValue
     */
    public addDisableValues(values: Array<string>): void {
        for (const value of values) {
            this.addDisableValue(value);
        }
    }

    /**
     * Clear disabled values and then add multiple new values to disable in one call.
     * @see addDisableValues
     */
    public setDisableValues(values: Array<string>): void {
        this.disableValues = [];
        this.addDisableValues(values);
    }

    /**
     * Remove disabled value list.
     */
    public removeDisableValue(value: string): void {
        value = value.trim();
        const index = this.disableValues.indexOf(value);
        if (-1 !== index) {
            this.disableValues.splice(index, 1);
            this.applyDisabledOption(value, false);
        }
    }

    /**
     * @see removeDisableValue
     */
    public removeDisableValues(values: Array<string>): void {
        for (const value of values) {
            this.removeDisableValue(value);
        }
    }

    public addListener(event: 'select'|'unselect', callback: (event: OptionManagerEvent<T>) => void): void {
        this.eventEmitter.addListener(event, callback);
    }

    private applyDisabledOption(value: string, disabled: boolean = true): void {
        const option = this.options.get(value);

        if (option) {
            option.disabled = disabled;
        }
    }

    private addOption(option: Option<T>): void {
        this.options.set(option.value, option);
        const $option = this.getHTMLOptionElementFor(option);

        if (option.selected) {
            this.order.push(option);
        }

        if (!this.multiple) {
            option.addListener('select', () => {
                for (const opt of this.options.values()) {
                    if (opt.selected && opt !== option) {
                        opt.selected = false;
                    }
                }
            });
        }

        option.addListener('select', () => {
            $option.selected = true;
            this.eventEmitter.emit('select', new OptionManagerEvent<T>(option, this.selected));
            this.order.push(option);
        });

        option.addListener('unselect', () => {
            $option.selected = false;
            this.eventEmitter.emit('unselect', new OptionManagerEvent<T>(option, this.selected));
            this.order.splice(this.order.indexOf(option), 1);
        });

        option.addListener('disable', () => {
            $option.disabled = true;
        });

        option.addListener('enable', () => {
            $option.disabled = false;
        });
    }
}
