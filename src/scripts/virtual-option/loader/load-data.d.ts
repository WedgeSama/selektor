import {VirtualOption} from '../virtual-option';

export interface LoadData<T extends object> {
    search: string;
    options: Array<VirtualOption<T>>;
    page: number;
    finished: boolean;
    limit?: number;
    trustData?: boolean;
}
