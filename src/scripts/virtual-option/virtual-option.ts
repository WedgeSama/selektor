import {Option} from '../option';
import {TextNode} from './text-node';
import {TemplateUtils} from '../templating';
import {EventEmitter} from '../event-emitter';

export class VirtualOption<T extends object> {
    readonly children: Array<VirtualOption<T>> = [];
    private searchableTexts: Array<TextNode> = [];
    private eventEmitter = new EventEmitter();

    constructor(
        readonly $element: HTMLElement,
        readonly option?: Option<T>,
    ) {
        for (const $text of TemplateUtils.queryAllTextNode(this.$element)) {
            const textNode = new TextNode($text);
            if (textNode.empty) {
                continue;
            }

            this.searchableTexts.push(textNode);
        }

        if (this.option) {
            this.selected = this.option.selected;

            this.option.addListener('select', () => {
                this.selected = true;
            });

            this.option.addListener('unselect', () => {
                this.selected = false;
            });

            this.option.addListener('disable', () => {
                this.disabled = true;
            });

            this.option.addListener('enable', () => {
                this.disabled = false;
            });

            this.$element.addEventListener('mouseover', () => {
                this.eventEmitter.emit('hover', this);
            });

            this.$element.addEventListener('click', () => {
                this.option.toggleSelected();
            });
        }
    }

    public filter(search: string = ''): boolean {
        let childrenHide = true;
        for (const $child of this.children) {
            if (!$child.filter(search)) {
                childrenHide = false;
            }
        }

        let hide = true;
        if (0 < search.length) {
            for (const textNode of this.searchableTexts) {
                if (textNode.contain(search)) {
                    hide = false;
                    textNode.mark(search);
                }
            }
        } else {
            hide = false;
            for (const textNode of this.searchableTexts) {
                textNode.reset();
            }
        }

        this.$element.hidden = hide && childrenHide;
        return this.$element.hidden;
    }

    public get hidden(): boolean {
        return this.$element.hidden;
    }

    public addChild(child: VirtualOption<T>): VirtualOption<T> {
        this.children.push(child);
        this.$element.appendChild(child.$element);

        return this;
    }

    public get horizontalChildren(): Array<VirtualOption<T>> {
        let options: Array<VirtualOption<T>> = [];
        for (const child of this.children) {
            options.push(child);
            options = options.concat(child.horizontalChildren);
        }

        return options;
    }

    public addListener(event: 'hover', callback: (option: VirtualOption<T>) => void): void {
        this.eventEmitter.addListener(event, callback);
    }

    public set focus(val: boolean) {
        if (val) {
            this.$element.classList.add('selektor-option-focus');
        } else {
            this.$element.classList.remove('selektor-option-focus');
        }
    }

    public click(): boolean {
        const selected = this.selected;
        this.$element.click();
        return !selected;
    }

    private set selected(val: boolean) {
        if (val) {
            this.$element.setAttribute('data-selektor-selected', '');
        } else {
            this.$element.removeAttribute('data-selektor-selected');
        }
    }

    private set disabled(val: boolean) {
        if (val) {
            this.$element.setAttribute('data-selektor-disabled', '');
        } else {
            this.$element.removeAttribute('data-selektor-disabled');
        }
    }
}
