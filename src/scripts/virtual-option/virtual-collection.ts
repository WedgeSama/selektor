import {LoaderInterface} from './loader/loader';
import {LoadData} from './loader/load-data';
import {VirtualOption} from './virtual-option';
import {TemplateEngine} from '../templating';

export class VirtualCollection<T extends object> {
    private $collection: HTMLElement;
    private lastLoadedData: LoadData<T>|null = null;
    private currentVirtualOptions: Array<VirtualOption<T>> = [];
    private currentFocusIndex: number|null = null;
    private $internalNoOption: HTMLElement|null = null;
    private currentPromise: Promise<void>|null = null;
    private isOpened: boolean = false;

    constructor(
        private templateEngine: TemplateEngine<T>,
        private loader: LoaderInterface<T>,
    ) {
        this.$collection = this.templateEngine.$virtualCollection;

        this.$collection.parentElement.addEventListener('scroll', (e) => {
            this.triggerNextLoad();
        });
    }

    set opened(value: boolean) {
        this.isOpened = value;
    }

    public filter(search: string = ''): void {
        search = search.normalize('NFD').trim().toLowerCase();
        this.currentPromise = null;
        this.$collection.innerHTML = '';
        this.lastLoadedData = null;
        this.currentFocusIndex = null;
        this.currentVirtualOptions = [];
        this.load(search);
    }

    public focusNext(): void {
        if (null === this.currentFocusIndex) {
            this.focusIndex(0);
        } else {
            this.focusIndex(this.currentFocusIndex + 1);
        }
    }

    public focusPrev(): void {
        if (null === this.currentFocusIndex) {
            this.focusIndex(this.horizontalVirtualOptions.length - 1);
        } else {
            this.focusIndex(this.currentFocusIndex - 1);
        }
    }

    public focusIndex(index: number): void {
        if (
            (0 < this.horizontalVirtualOptions.length) &&
            (index >= 0) &&
            (index < this.horizontalVirtualOptions.length)
        ) {
            if (null !== this.currentFocusIndex) {
                this.currentFocus.focus = false;
            }

            this.currentFocusIndex = index;
            this.currentFocus.focus = true;

            const $wrapperBounds = this.$collection.parentElement.getBoundingClientRect();
            const $optBounds = this.currentFocus.$element.getBoundingClientRect();

            if (
                $wrapperBounds.top > $optBounds.top ||
                $wrapperBounds.bottom < $optBounds.bottom
            ) {
                this.currentFocus.$element.scrollIntoView();
            }
        }
    }

    public focusFirst(): void {
        this.focusIndex(0);
    }

    public focusLast(): void {
        this.focusIndex(this.horizontalVirtualOptions.length - 1);
    }

    /**
     * Select or unselect focused option. Return true if select, false if unselect.
     */
    public triggerFocused(): boolean {
        const option = this.currentFocus;
        if (null !== option) {
            return option.click();
        }

        return false;
    }

    private get currentFocus(): VirtualOption<T>|null {
        if ((0 === this.horizontalVirtualOptions.length) || (null === this.currentFocusIndex)) {
            return null;
        }

        return this.horizontalVirtualOptions[this.currentFocusIndex] ?? null;
    }

    private get horizontalVirtualOptions(): Array<VirtualOption<T>> {
        let virtualOptions: Array<VirtualOption<T>> = [];
        for (const virtualOption of this.currentVirtualOptions) {
            if (!virtualOption.option.disabled) {
                virtualOptions.push(virtualOption);
            }

            virtualOptions = virtualOptions.concat(virtualOption.horizontalChildren);
        }

        return virtualOptions;
    }

    private get empty(): boolean {
        return 0 === this.horizontalVirtualOptions.filter((opt: VirtualOption<T>) => {
            return !opt.hidden;
        }).length;
    }

    private load(search: string = '', page: number = 1): void {
        this.templateEngine.startLoading();
        const promise = this.loader.load(search, page).then((loadData: LoadData<T>) => {
            if (promise !== this.currentPromise) {
                return;
            }

            this.lastLoadedData = loadData;
            for (const opt of loadData.options) {
                if (!loadData.trustData){
                    opt.filter(search);
                }
                this.$collection.appendChild(opt.$element);
                opt.addListener('hover', () => {
                    if (!opt.option.disabled) {
                        this.focusIndex(this.horizontalVirtualOptions.indexOf(opt));
                    }
                });
            }

            this.currentVirtualOptions = this.currentVirtualOptions.concat(loadData.options);
            if (!loadData.finished) {
                this.triggerNextLoad();
            }

            if (this.empty) {
                this.$collection.appendChild(this.$noOption);
            } else {
                this.$noOption.remove();
            }
        }).finally(() => {
            this.templateEngine.endLoading();
        }).catch(() => {
            this.templateEngine.endLoading();
        });

        this.currentPromise = promise;
    }

    private triggerNextLoad(): void {
        if (!this.isOpened || null === this.lastLoadedData || this.lastLoadedData.finished || this.loader.loading) {
            return;
        }

        if (this.$collection.parentElement.getBoundingClientRect().bottom > this.$collection.getBoundingClientRect().bottom - 10) {
            this.load(this.lastLoadedData.search, this.lastLoadedData.page + 1);
        }
    }

    private get $noOption(): HTMLElement {
        if (null === this.$internalNoOption) {
            this.$internalNoOption = this.templateEngine.renderNoOption();
        }

        return this.$internalNoOption;
    }
}
