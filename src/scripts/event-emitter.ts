export class EventEmitter {
    private listeners: Map<string, Array<(...args) => void>> = new Map();

    public addListener(event: string, callback: (...args) => void): void {
        if (!this.listeners.has(event)) {
            this.listeners.set(event, []);
        }

        this.listeners.get(event).push(callback);
    }

    public emit(name: string, event?: any): void {
        if (this.listeners.has(name)) {
            for (const callback of this.listeners.get(name)) {
                callback(event);
            }
        }
    }
}
