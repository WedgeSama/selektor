import {TemplateEvent} from './template-event';
import {Option} from '../../option';

export class OptionTemplateEvent<T extends object> extends TemplateEvent {
    constructor(
        $element: HTMLElement,
        readonly option: Option<T>,
    ) {
        super($element);
    }
}
