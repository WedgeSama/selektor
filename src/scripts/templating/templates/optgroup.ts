export default (label: string): string => {
    return `
<ul class="selektor-list">
    <li class="selektor-optgroup" data-selektor-optgroup>${label}</li>
</ul>
    `;
};
