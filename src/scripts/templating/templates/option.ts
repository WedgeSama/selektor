import {Option} from '../../option';

export default (option: Option<object>): string => {
    return `
<li class="selektor-option" data-selektor-option="${option.value}"${option.selected ? ' data-selektor-selected' : ''}${option.disabled ? ' data-selektor-disabled' : ''}>
    ${option.label}
</li>
    `;
};
