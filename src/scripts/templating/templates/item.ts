import {Option} from '../../option';

export default (option: Option<object>): string => {
    return `
<span class="selektor-item">
    ${option.label}
    <button type="button" class="selektor-item-clear" data-selektor-item-delete>
        x
    </button>
</span>
`;
};
