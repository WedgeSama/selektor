import mainTemplate from './main';
import optionTemplate from './option';
import optgroupTemplate from './optgroup';
import itemTemplate from './item';
import nooptionTemplate from './no-option';

export default {
    main: mainTemplate,
    option: optionTemplate,
    optgroup: optgroupTemplate,
    item: itemTemplate,
    nooption: nooptionTemplate,
}

export {
    mainTemplate,
    optionTemplate,
    optgroupTemplate,
    itemTemplate,
    nooptionTemplate,
}
