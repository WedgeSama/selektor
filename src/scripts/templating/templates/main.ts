export default (cssClasses: string|null = null): string => {
    return `
<div class="selektor${cssClasses ? ' ' + cssClasses : ''}">
    <div class="selektor-zone" data-selektor-item-collection>
        <input class="selektor-filter" data-selektor-input>
    </div>

    <div class="selektor-virtual-collection-wrapper" data-selektor-virtual-collection-wrapper>
        <ul class="selektor-virtual-collection" data-selektor-virtual-collection></ul>
        <div class="selektor-virtual-loading" data-selektor-virtual-loading hidden></div>
    </div>
</div>
`;
};
