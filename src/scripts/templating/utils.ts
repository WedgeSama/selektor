export abstract class Utils {
    private constructor() {}

    public static stringToHTML<T extends ChildNode = ChildNode>(str: string): T {
        const $template = document.createElement('template');
        $template.innerHTML = str.trim();
        return $template.content.firstChild as T;
    }

    public static stringToHTMLs(str: string): NodeListOf<ChildNode> {
        const $template = document.createElement('template');
        $template.innerHTML = str.trim();
        return $template.content.childNodes;
    }

    public static insertAfter($insertedNode: Node|{[Symbol.iterator]}, $referenceNode: Node): void {
        if (Utils.isIterable($insertedNode)) {
            let $loopRef = $referenceNode;
            for (const $node of Array.from<Node>($insertedNode)) {
                Utils.insertAfter($node, $loopRef);
                $loopRef = $node;
            }
        } else {
            $referenceNode.parentNode.insertBefore($insertedNode, $referenceNode);
            $referenceNode.parentNode.insertBefore($referenceNode, $insertedNode);
        }
    }

    public static insertBefore($insertedNode: Node|{[Symbol.iterator]}, $referenceNode: Node): void {
        if (Utils.isIterable($insertedNode)) {
            let $loopRef = $referenceNode;
            for (const $node of Array.from<Node>($insertedNode)) {
                Utils.insertBefore($node, $referenceNode);
                $loopRef = $node;
            }
        } else {
            $referenceNode.parentNode.insertBefore($insertedNode, $referenceNode);
        }
    }

    public static detach<T extends Node>($node: T): T {
        const $parent = $node.parentNode;

        if (null === $parent) {
            throw new Error('Cannot detach an already detached Node.');
        }

        return $parent.removeChild($node);
    }

    public static contained($ancestor: Element, $child: Element): boolean {
        if ($ancestor === $child.parentElement) {
            return true;
        }

        if (null === $child.parentElement) {
            return false;
        }

        return Utils.contained($ancestor, $child.parentElement);
    }

    public static queryAllTextNode($element: HTMLElement): Array<Text> {
        // TODO Handle the deprecated notice.
        // @ts-ignore
        const walker = document.createTreeWalker($element, NodeFilter.SHOW_TEXT, null, false);
        const arr: Array<Text> = [];
        let node;

        while (node = walker.nextNode()) {
            arr.push(node);
        }

        return arr;
    }

    private static isIterable(variable): variable is {[Symbol.iterator]} {
        return (null !== variable) &&
            (Symbol.iterator in Object(variable))
        ;
    }
}
