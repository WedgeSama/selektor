import {Configuration, ConfigurationDomLoader, Validator, ValidConfiguration} from './configuration';
import {TemplateEngine, TemplateUtils} from './templating';
import {OptionManager} from './option/option-manager';
import {LoaderInterface, VirtualCollection, VirtualOptionDomLoader, VirtualOptionRemoteLoader} from './virtual-option';
import {ItemCollection} from './item/item-collection';
import {OptionManagerEvent} from './option';
import {Option} from './option';
import {Data} from './data';
import {TemplateEvent} from './templating/event/template-event';
import {OptionTemplateEvent} from './templating/event/option-template-event';
import {BuilderRegister, ConfiguratorBuilder} from './configuration/builder/builder-register';

export class Selektor<T extends object> {
    private $select: HTMLSelectElement;
    private config: ValidConfiguration;
    private templateEngine: TemplateEngine<T>;
    private optionManager: OptionManager<T>;
    private virtualCollection: VirtualCollection<T>;
    private itemCollection: ItemCollection<T>;

    private isOpened: boolean = false;

    private static configBuilderRegister: BuilderRegister = new BuilderRegister();

    constructor(
        $select: HTMLSelectElement|string,
        config: Configuration|null = null,
    ) {
        this.$select = Selektor.queryElement<HTMLSelectElement>($select, HTMLSelectElement);

        if (Selektor.getSelektorFor(this.$select)) {
            throw new Error(`Selektor already exist for "${this.$select.id}"`);
        }

        if (null === config) {
            config = Selektor.configBuilderRegister.buildConfiguration(this.$select);
        }

        this.config = Validator.validate(ConfigurationDomLoader.load(this.$select, config ?? {}));
        this.templateEngine = new TemplateEngine(this.$select, this.config.templates);
        this.templateEngine.$input.placeholder = this.config.placeholder;
        this.optionManager = new OptionManager<T>(this.$select, this.config.multiple);

        let loader: LoaderInterface<T>;
        if (this.config.remote) {
            loader = new VirtualOptionRemoteLoader(this.templateEngine, this.optionManager, this.config.remote);
        } else if (this.config.virtualLoader) {
            loader = this.config.virtualLoader(this.$select, this.templateEngine, this.optionManager);
        } else {
            loader = new VirtualOptionDomLoader<T>(this.$select, this.templateEngine, this.optionManager);
        }

        this.virtualCollection = new VirtualCollection<T>(this.templateEngine, loader);
        this.itemCollection = new ItemCollection<T>(this.templateEngine, this.optionManager);

        this.applyEventListenerToInterfaces();
        this.applyInternalListeners();
        this.refreshPlaceholder();
        (this.$select as any).selektor = this;
    }

    public static getSelektorFor<U extends object>($select: HTMLSelectElement|string): Selektor<U>|null {
        $select = Selektor.queryElement<HTMLSelectElement>($select, HTMLSelectElement);

        if ($select.hasOwnProperty('selektor')) {
            const selektor = ($select as any).selektor;

            if (selektor instanceof Selektor) {
                return selektor;
            }
        }

        return null;
    }

    public static scanHTMLSection($section: HTMLElement|string = document.body): void {
        $section = Selektor.queryElement<HTMLElement>($section, Element);
        if ($section instanceof HTMLSelectElement) {
            if (null === Selektor.getSelektorFor($section)) {
                new Selektor($section);
            }
        } else {
            for (const $select of $section.querySelectorAll<HTMLSelectElement>('select[data-selektor]')) {
                if (null === Selektor.getSelektorFor($select)) {
                    new Selektor($select);
                }
            }
        }
    }

    public static registerConfigBuilder(builder: ConfiguratorBuilder): void {
        Selektor.configBuilderRegister.addBuilder(builder);
    }

    public set opened(opened: boolean) {
        this.isOpened = opened;

        if (opened) {
            this.templateEngine.$render.classList.add('opened');
            this.virtualCollection.filter(this.templateEngine.$input.value);
        } else {
            this.templateEngine.$render.classList.remove('opened');
            this.templateEngine.$input.blur();
            this.templateEngine.$input.value = '';
        }

        this.virtualCollection.opened = opened;
    }

    public get opened(): boolean {
        return this.isOpened;
    }

    public addDisableValue(value: string): Selektor<T> {
        this.optionManager.addDisableValue(value);
        return this;
    }

    public addDisableValues(values: Array<string>): Selektor<T> {
        this.optionManager.addDisableValues(values);
        return this;
    }

    public setDisableValues(values: Array<string>): Selektor<T> {
        this.optionManager.setDisableValues(values);
        return this;
    }

    public removeDisableValue(value: string): Selektor<T> {
        this.optionManager.removeDisableValue(value);
        return this;
    }

    public removeDisableValues(values: Array<string>): Selektor<T> {
        this.optionManager.removeDisableValues(values);
        return this;
    }

    public addListener(event: 'select'|'unselect', callback: (event: OptionManagerEvent<T>) => void): void;
    public addListener(event: 'main-template'|'optgroup-template'|'nooption-template', callback: (event: TemplateEvent) => void): void;
    public addListener(event: 'option-template'|'item-template', callback: (event: OptionTemplateEvent<T>) => void): void;
    public addListener(event: string, callback: (...args: any) => void): void {
        if ('select' === event || 'unselect' === event) {
            this.optionManager.addListener(event, callback);
        } else if ('option-template' === event || 'item-template' === event) {
            this.templateEngine.addListener(event, callback);
        } else if ('main-template' === event || 'optgroup-template' === event || 'nooption-template' === event) {
            this.templateEngine.addListener(event, callback);
        }
    }

    public get selected(): Array<Data<T>>|Data<T>|null {
        const selected = this.optionManager.selected;

        if (null === selected) {
            return null;
        }

        if (Array.isArray(selected)) {
            return selected.map(option => this.optionToData(option));
        }

        return this.optionToData(selected);
    }

    public set selected(selected: Array<Data<T>>|Data<T>|null) {
        this.optionManager.clear();

        if (null !== selected) {
            this.addSelected(selected);
        }
    }

    public addSelected(selected: Array<Data<T>>|Data<T>): void {
        if (Array.isArray(selected)) {
            for (const data of selected) {
                this.dataToOption(data);
            }
        } else if (null !== selected) {
            this.dataToOption(selected);
        }
    }

    /**
     * Apply event listener to all needed interface elements.
     * @private
     */
    private applyEventListenerToInterfaces(): void {
        // Listener for keyboard shortcut.
        this.templateEngine.$render.addEventListener('keydown', (e: KeyboardEvent) => {
            if ('ArrowDown' === e.key) {
                e.preventDefault();
                this.virtualCollection.focusNext();
            } else if ('ArrowUp' === e.key) {
                e.preventDefault();
                this.virtualCollection.focusPrev();
            } else if ('Enter' === e.key) {
                e.preventDefault();
                this.virtualCollection.triggerFocused();
            } else if ('Home' === e.key) {
                e.preventDefault();
                this.virtualCollection.focusFirst();
            } else if ('End' === e.key) {
                e.preventDefault();
                this.virtualCollection.focusLast();
            } else if ('Backspace' === e.key && 0 === this.templateEngine.$input.value.length) {
                this.optionManager.unselectLast();
            }
        });

        document.addEventListener('keydown', (e: KeyboardEvent) => {
            if (this.opened) {
                if ('Escape' === e.key) {
                    this.opened = false;
                    this.templateEngine.$input.value = '';
                } else if (document.activeElement !== this.templateEngine.$input) {
                    this.templateEngine.$input.focus();
                }
            }
        });

        // Listeners to handle closing of dropdown.
        let clickedIn = false;
        const globalClick = (e: MouseEvent): void => {
            if (e.target instanceof Element) {
                this.templateEngine.$input.focus();
                clickedIn = TemplateUtils.contained(this.templateEngine.$virtualCollectionWrapper, e.target);
            }
        };

        this.templateEngine.$input.addEventListener('focus', () => {
            if (!this.opened) {
                this.opened = true;
                document.addEventListener('mousedown', globalClick);
            }
        });

        this.templateEngine.$input.addEventListener('blur', () => {
            if (!clickedIn) {
                this.opened = false;
                document.removeEventListener('mousedown', globalClick);
            }
        });

        // Listener for filtration input.
        this.templateEngine.$input.addEventListener('input', () => {
            this.virtualCollection.filter(this.templateEngine.$input.value);
        });

        // Listeners for labels' click.
        for (const $label of this.templateEngine.$labels) {
            $label.addEventListener('click', () => {
                this.templateEngine.$input.focus();
            });
        }
    }

    private applyInternalListeners(): void {
        this.optionManager.addListener('select', () => {
            if (!this.config.multiple) {
                this.opened = false;
                this.templateEngine.$input.value = '';
            }

            this.triggerNativeSelectChange();
            this.refreshPlaceholder();
        });

        this.optionManager.addListener('unselect', () => {
            this.triggerNativeSelectChange();
            this.refreshPlaceholder();
        });
    }

    private optionToData(option: Option<T>): Data<T> {
        return {
            label: option.label,
            value: option.value,
            data: option.data,
        };
    }

    private dataToOption(data: Data<T>): Option<T> {
        return this.optionManager.getOptionFor(data.value, data.label, true, data.data);
    }

    private triggerNativeSelectChange(): void {
        const event = document.createEvent('HTMLEvents');
        event.initEvent('change', false, true);
        this.$select.dispatchEvent(event);
    }

    private refreshPlaceholder(): void {
        if (!this.config.multiple) {
            const selected = this.optionManager.selected;
            if ((null === selected) || (Array.isArray(selected) && 0 === selected.length)) {
                this.templateEngine.$input.placeholder = this.config.placeholder;
            } else {
                this.templateEngine.$input.placeholder = '';
            }
        }
    }

    private static queryElement<U extends HTMLElement>($elem: U|string, type?: Function): U {
        if ('string' === typeof $elem) {
            const $found = document.querySelector<U>($elem);

            if (null === $found) {
                throw new Error(`Cannot found any HTML element with selector "${$elem}"`);
            }

            if (type && !($found instanceof type)) {
                throw new Error(`The element found with "${$elem}" must be an instance of ${type.prototype}.`);
            }

            return $found;
        }

        return $elem;
    }
}
