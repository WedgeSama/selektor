Selektor
========

Selektor is a lite-weight `<select>` UI alternative. It is based on native HTMLSelectElement. You can:
- Use your owns templates or one based on your favorite UI framework.
- Search/filter over select's options.
- Navigate by keyboard (arrows, paragraphs, escape, enter, etc...)
- Programmatically disable options.
- Load options from remote source (API, Ajax, etc...).
- Usable in your JavaScript and TypeScript projects!

## Install

### Packages managers
```shell
npm install @selektor/selektor
# or
yarn add @selektor/selektor
```

## Basic usages

```js
import {Selektor} from '@selektor/selektor';

// If you want to init existing selektor by data attribute.
Selektor.scanHTMLSection();

// Same as previous but limit on a part of the document.
Selektor.scanHTMLSection('#your-section-id');
Selektor.scanHTMLSection(document.querySelector('#your-section-id'));

// Manually init Selektor.
const select = new Selektor('select#your-select-id');
const select = new Selektor(document.querySelector('select#your-select-id'));

// With config.
const select = new Selektor('select#your-select-id', {
    // Put your config here
});

```

## Configuration

See [configuration](src/scripts/configuration/configuration.d.ts) source code.

## Advanced Usages

### Custom templates

There are 5 templates customizable. Take a look at [default templates directory](src/scripts/templating/templates) to see what you can do.
Here an example with the default template for virtual option in the dropdown.
```ts
const select = new Selektor('select#your-select-id', {
    templates: {
        option: (option: Option<object>): string => {
            return `
<li class="selektor-option" data-selektor-option="${option.value}"${option.selected ? ' data-selektor-selected' : ''}${option.disabled ? ' data-selektor-disabled' : ''}>
    ${option.label}
</li>
    `;
        },
        // main: ...,
        // optgroup: ...,
        // item: ...,
        // nooption: ...,
    },
});
```

### Remote options

#### Basic exemple

```js
// Using default remote loader.
const select = new Selektor('select#your-select-id', {
    remote: 'https://url.to.your.domain/path/to/resource',
});
```

The data returned by your remote must be an array of string or an array of object like this:
```js
const obj = {
    value: 'string',
    label: 'string',
};

```

#### Custom search

This example is the default config.
```ts
const select = new Selektor('select#your-select-id', {
    remote: {
        url: 'https://url.to.your.domain/path/to/resource',
        queryBuilder: (search: string, page: number): string => {
            return `page=${page}&search=${encodeURI(search)}`;
        },
    },
});
```

#### Custom value and label field for default remote loader.

```js
const select = new Selektor('select#your-select-id', {
    remote: {
        url: 'https://url.to.your.domain/path/to/resource',
        valueField: 'id',
        labelField: 'title',
    },
});
```

## License

This bundle is under the MIT license. See the complete [LICENSE](LICENSE).
