const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");

const scriptsConfig = (env, argv) => {
    const mode = argv.mode ? argv.mode : 'development';
    return {
        mode: argv.mode ? argv.mode : 'development',
        entry: {
            'selektor': path.resolve(__dirname, 'src', 'scripts', 'index.ts'),
        },
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: '[name].js',
            libraryTarget: 'umd',
            library: 'selektor',
            umdNamedDefine: true,
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
        },
        devtool: 'development' === mode ? 'source-map' : false,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules|bundles|esm5|esm2015/,
                },
            ],
        },
    };
};

const stylesConfig = (env, argv) => {
    const mode = argv.mode ? argv.mode : 'development';
    return {
        mode: mode,
        entry: {
            'selektor': path.resolve(__dirname, 'src', 'styles', 'index.scss'),
        },
        output: {
            path: path.resolve(__dirname, 'dist'),
        },
        resolve: {
            extensions: ['.css', '.sass', '.scss'],
        },
        devtool: 'development' === mode ? 'source-map' : false,
        plugins: [
            new FixStyleOnlyEntriesPlugin(),
            new MiniCssExtractPlugin({
                filename: '[name].css',
            }),
        ],
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        'sass-loader',
                    ],
                },
            ],
        },
    };
};

module.exports = [
    scriptsConfig,
    stylesConfig,
];
