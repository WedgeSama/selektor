const fs = require('fs');
const path = require('path');
const copyDir = require('copy-dir');

const args = process.argv.slice(2);

if (0 === args.length) {
    console.error('Missing version argument.');
    process.exit(1);
}

if (1 < args.length) {
    console.error('Only one argument: version.');
    process.exit(2);
}

const sourcePackageJson = require('./package.json');
const releasePackageJson = {
    name: 'selektor',
    version: args[0],
};

const copyProperties = [
    'name',
    'description',
    'repository',
    'author',
    'keywords',
    'license',
    'bugs',
    'homepage',
    'dependencies',
];

for (let copyProperty of copyProperties) {
    if (sourcePackageJson.hasOwnProperty(copyProperty)) {
        releasePackageJson[copyProperty] = sourcePackageJson[copyProperty];
    }
}

// Copy sources.
copyDir.sync(path.resolve(__dirname, 'src'), path.resolve(__dirname, 'dist', 'src'));

// Copy files.
fs.copyFileSync(path.resolve(__dirname, 'LICENSE'), path.resolve(__dirname, 'dist', 'LICENSE'));
fs.copyFileSync(path.resolve(__dirname, 'README.md'), path.resolve(__dirname, 'dist', 'README.md'));

// Main and equivalent config.
if (fs.existsSync(path.resolve(__dirname, 'dist', 'selektor.js'))) {
    releasePackageJson.main = './selektor.js';
}

if (fs.existsSync(path.resolve(__dirname, 'dist', 'src'))) {
    releasePackageJson.source = './src/scripts/index.ts';
    releasePackageJson.sass = './src/styles/index.scss';
}

if (fs.existsSync(path.resolve(__dirname, 'dist', 'esm2015'))) {
    releasePackageJson.esm2015 = './esm2015/index.js';
}

if (fs.existsSync(path.resolve(__dirname, 'dist', 'esm5'))) {
    releasePackageJson.esm5 = './esm5/index.js';
    releasePackageJson.typings = './esm5/index.d.ts';
}

if (fs.existsSync(path.resolve(__dirname, 'dist', 'selektor.css'))) {
    releasePackageJson.style = './selektor.css';
}

// Write package.json.
fs.writeFileSync(path.resolve(__dirname, 'dist', 'package.json'), JSON.stringify(releasePackageJson, null, 4) + '\n');
